<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>学生信息表</title>
	<link rel="stylesheet" href="layui/css/layui.css">
	<link rel="stylesheet" href="soulTable.css" media="all"/>
	<script src="layui/layui.js"></script>
	
</head>
<body>
<%!
	String url="StudentDataServlet";
	String operation ="queryAll";
	String name="";
	String studentId="";
%>
<%
	request.setCharacterEncoding("UTF-8");
	operation=request.getParameter("operation");
	name=request.getParameter("name");
	studentId=request.getParameter("studentId");
	if(!("queryAll".equals(operation)||("".equals(operation)))){//如果是条件查询
		url=String.format("StudentDataServlet?operation=%s&name=%s&studentId=%s",operation,name,studentId);
	}
%>

	<h2 align=center>学生信息表</h2>
	<table id="myTable" lay-filter="studentInfo"></table>
	<script type="text/html" id="toolbar">
		<div><button class="layui-btn layui-btn-sm" lay-event="clearFilter">清除所有筛选条件</button></div>
	</script>
	<script type="text/html" id="toolbar2">
		<a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
	</script>
	<script type="text/javascript" src="layui/layui.js"></script>
	<script>
		layui.config({
		    base: 'ext/',   // 模块目录
		}).extend({                         
		    soulTable: 'soulTable'  // 模块别名
		});
	
		layui.use(['form', 'table','soulTable'], function () {
	   	var table = layui.table,
	  	soulTable = layui.soulTable;
	   	
	   	
	   	// 后台分页
	   	table.render({
			elem: '#myTable'
		    ,id: 'myTable'
		    //url: 'student_data.json'
		    ,url: '<%=url %>'
		    ,height: '500'
		    ,toolbar: '#toolbar1'
		    ,page: true
		    ,cols: [[
			  {type: 'checkbox', fixed: 'left'},
			  {field: 'studentId', title:'学号', width: 150, sort: true, filter: true},
		      {field: 'name', title: '姓名', width: 110, sort: true, filter: true},
		      {field: 'genderString', title: '性别', width: 120, sort: true, filter: true},
		      {field: 'classId', title: '班级', width: 120, sort: true, filter: true},
		      {field: 'major', title: '专业', width: 100 , filter: true},
		      {field: 'type', title: '学生类别', width: 120, sort: true, filter: true},
		      {field: 'enrollDate', title: '入学日期', width: 150,  filter: true},
		      {field: 'academicStructure', title:'学制', width: 120, filter: true},
		      {field: 'campus', title: '所属校区', width: 110,  filter: true},
		      {title: '操作', width: 120, fixed: 'right', toolbar: '#toolbar2'}
		    ]]
	   		/*解析函数不必存在
	   		,parseData: function(res){ //res 即为原始返回的数据
	   			//不必进行转换，直接使用一同打包的genderString属性即可
		   	    for(var i=0;i<res.data.length;i++){
		   	    	if(res.data[i].gender){
		   	    		res.data[i].gender="男";
		   	    	}
		   	    	else{
		   	    		res.data[i].gender="女";
		   	    	}
		   	    }
	   			
	   			return {
		   	      "code": res.code, //解析接口状态
		   	      "msg": res.msg, //解析提示文本
		   	      "count": res.count, //解析数据长度
		   	      "data": res.data //解析数据列表
		   	    };
	   	  	}
	  	 	*/
		    ,done: function () {
		      soulTable.render(this)
		    }
		  });
		  
		  table.on('toolbar(studentInfo)', function(obj){
		  	if (obj.event === 'clearFilter') {
		      // 清除所有筛选条件并重载表格
		      // 参数: tableId
		      //layer.alert('触发1');
		      soulTable.clearFilter('myTable')
		    }
		  });
		  
		  table.on('tool(studentInfo)', function(obj){
			    var data = obj.data;
			    var httpRequest = new XMLHttpRequest();//第一步：创建需要的对象
			    if(obj.event === 'del'){
			      layer.confirm('确定删除此行数据吗？', function(index){
					    httpRequest.open('POST', 'StudentDataServlet', true); //第二步：打开连接/***发送json格式文件必须设置请求头 ；如下 - */
						httpRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
					    
				        obj.del();
				        httpRequest.send('operation=del&studentId='+data.studentId); 
				        layer.close(index);
			      });
			    } else if(obj.event === 'edit'){
			    	
			    	var para="";
				    for(var x in data){
				    	if(para!==""){
				    		//如果不是第一个参数
				    		para += "&";//加上连接符
				    	}
				    	para += String(x+"="+data[x]);
				    }
				    para += "&operation=mod";//添加操作参数
			        
			        window.location.href="student_update.jsp?"+para;//跳转    
	
			    }
		  });
		})
	</script>
	
</body>
</html>