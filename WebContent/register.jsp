<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<title>注册</title>
	<!-- 引入示例，每个用到layui的文件在头部加上这两行 -->
	<link rel="stylesheet" href="layui/css/layui.css">
	<script src="layui/layui.js"></script>
	<script>
		layui.use('form', function() {
			var form = layui.form;
		});
	</script>
</head>
<body>

	<div class="layui-row layui-col-space10">
		<div class="layui-col-md4"></div>
		<div class="layui-col-md4">
			<div style="margin: 5px; padding: 5px; width: 400px" align=center>
				<h1>新用户注册</h1>
			</div>
			
			
			<form action="AccountVerifyServlet" method="POST" class="layui-form">
				<input type="hidden" name="method" value="register">
				<div class="layui-form-item" style="width: 400px">
					<label class="layui-form-label">用户名</label>
					<div class="layui-input-block">
						<input type="text" name="username" required lay-verify="required"
							placeholder="请输入用户名" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item" style="width: 400px">
					<label class="layui-form-label">密码</label>
					<div class="layui-input-block">
						<input type="password" name="password1" required
							lay-verify="required" placeholder="请输入密码" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item" style="width: 400px">
					<label class="layui-form-label">确认密码</label>
					<div class="layui-input-block">
						<input type="password" name="password2" required
							lay-verify="required" placeholder="请再次输入密码" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit lay-filter="formDemo">注册</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
		<div class="layui-col-md4"></div>
	</div>

</body>
</html>