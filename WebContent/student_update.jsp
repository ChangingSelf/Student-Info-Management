<%@page import="javax.security.auth.message.callback.PrivateKeyCallback.Request"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>学生信息更新</title>
	<link rel="stylesheet" href="layui/css/layui.css">
	<link rel="stylesheet" href="soulTable.css" media="all"/>
	<script src="layui/layui.js"></script>
</head>
<body>

<%!
	String operation ="add";
%>

<%
	request.setCharacterEncoding("UTF-8");
	operation = request.getParameter("operation");
	if(operation==null) operation="add";

	String studentId = request.getParameter("studentId");
	String name = request.getParameter("name");
	String genderString = request.getParameter("gender");
	String classId = request.getParameter("classId");
	String major = request.getParameter("major");
	String type = request.getParameter("type");
	String enrollDateString = request.getParameter("enrollDate");
	String academicStructureString = request.getParameter("academicStructure");
	String campus = request.getParameter("campus");
	
	//对每个参数进行预处理，将null的参数置为空字符串
	if(operation==null) operation="";//如果操作类型为空，则直接返回，不进行操作
	if(studentId==null) studentId="";
	if(name==null) name="";
	if(genderString==null) genderString="";
	if(classId==null) classId="";
	if(major==null) major="";
	if(type==null) type="";
	if(enrollDateString==null) enrollDateString="2020-06-09";
	if(academicStructureString==null) academicStructureString="0";
	if(campus==null) campus="";
	

%>
<script>
layui.use('form', function(){
	  var form = layui.form;
	  
	});

layui.use('laydate', function(){
  var laydate = layui.laydate;
  
  //执行一个laydate实例
  laydate.render({
    elem: '#enrollDate' //指定元素
    ,value: <%="".equals(enrollDateString)?"new Date()":"".format("'%s'", enrollDateString)%>
  });
});
</script>
	<h2 align=center>学生信息更新</h2>
	<hr />
	<br />
	<fieldset class="layui-elem-field">
		<legend>学生信息</legend>
		<form class="layui-form" name="base-number" action="StudentDataServlet">
			<input type="hidden" name="operation" value="<%=operation%>">
			<br />
			<div class="layui-form-item">
			    <label class="layui-form-label">学号</label>
			    <div class="layui-input-block">
			    	<input type="text" name="studentId" required  
			    	lay-verify="required" 
			    	placeholder='<%="".equals(studentId)?"请输入学号":studentId %>' 
			    	autocomplete="off" class="layui-input"
			    	value='<%="".equals(studentId)?"":studentId %>' 
			    	<%="".equals(studentId)?"":"readonly" %>>
			    </div>
	  		</div>
	  		
	  		<div class="layui-form-item">
			    <label class="layui-form-label">姓名</label>
			    <div class="layui-input-block">
			    	<input type="text" name="name" required  lay-verify="required" 
			    	placeholder='<%="".equals(name)?"请输入姓名":name %>' 
			    	autocomplete="off" class="layui-input" 
			    	value='<%="".equals(name)?"":name %>'>
			    </div>
	  		</div>
	  		
	  		<div class="layui-form-item">
			    <label class="layui-form-label">行政班级</label>
			    <div class="layui-input-block">
			    	<input type="text" name="classId" required  lay-verify="required" 
			    	placeholder='<%="".equals(classId)?"请输入班级号":classId %>' 
			    	autocomplete="off" class="layui-input" 
			    	value='<%="".equals(classId)?"":classId %>'>
			    </div>
	  		</div>
	  		
	  		<div class="layui-form-item">
			    <label class="layui-form-label">性别</label>
			    <div class="layui-input-block">
			      <input type="radio" name="gender" value="男" title="男" <%="true".equals(genderString)?"checked":"" %>>
			      <input type="radio" name="gender" value="女" title="女" <%="false".equals(genderString)?"checked":"" %>>
			    </div>
		  	</div>
	  		
	  		<div class="layui-form-item">
			    <label class="layui-form-label">学生类别</label>
			    <div class="layui-input-block">
			      <select name="type" lay-verify="required">
			        <option value="">请选择学生类别</option>
			        <option value="本科生" <%="本科生".equals(type)?"selected":"" %>>本科生</option>
			        <option value="研究生" <%="研究生".equals(type)?"selected":"" %>>研究生</option>
			      </select>
			    </div>
			</div>
			
			<div class="layui-form-item">
			    <label class="layui-form-label">学制</label>
			    <div class="layui-input-block">
			      <select name="academicStructure" lay-verify="required">
			        <option value="">请选择学生学制</option>
			        <option value="3" <%="3".equals(academicStructureString)?"selected":"" %>>三年制</option>
			        <option value="4" <%="4".equals(academicStructureString)?"selected":"" %>>四年制</option>
			      </select>
			    </div>
			</div>
	
	  		<div class="layui-form-item">
			    <label class="layui-form-label">入学日期</label>
			    <div class="layui-input-block">
			    	<input id="enrollDate" type="text" name="enrollDate" required  lay-verify="required" autocomplete="off" class="layui-input" >
			    </div>
	  		</div>
	  		
	  		
	  		
	  		<div class="layui-form-item">
			    <label class="layui-form-label">专业</label>
			    <div class="layui-input-block">
			    	<input type="text" name="major" required  lay-verify="required" 
			    	placeholder='<%="".equals(major)?"请输入专业":major %>' 
			    	autocomplete="off" class="layui-input" 
			    	value='<%="".equals(major)?"":major %>'>
			    	
			    </div>
	  		</div>
	  			
	  		<div class="layui-form-item">
			    <label class="layui-form-label">所属校区</label>
			    <div class="layui-input-block">
			      <select name="campus" lay-verify="required">
			        <option value="">请选择所属校区</option>
			        <option value="渭水校区" <%="渭水校区".equals(campus)?"selected":"" %>>渭水校区</option>
			        <option value="校本部" <%="校本部".equals(campus)?"selected":"" %>>校本部</option>
			        <option value="研究生院" <%="研究生院".equals(campus)?"selected":"" %>>研究生院</option>
			      </select>
			    </div>
			</div>	
	  		<br />
	  		<div class="layui-form-item">
			 	<div class="layui-input-block">
				<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		</form>
	</fieldset>

</body>
</html>