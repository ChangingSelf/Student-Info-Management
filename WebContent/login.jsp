<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>登录</title>
	<!-- 引入示例，每个用到layui的文件在头部加上这两行 -->
	<link rel="stylesheet" href="layui/css/layui.css">
	<script src="layui/layui.js"></script>
	<script>
		layui.use('form', function() {
			var form = layui.form;
		});
	</script>
</head>
<body>
	<%!
		String usernameMsg = "";//用户名输入框提示信息
		String passwordMsg = "";//密码输入框提示信息
	%>
	<%
		usernameMsg = (String)session.getAttribute("usernameMsg");
		passwordMsg = (String)session.getAttribute("passwordMsg");
	
		if(usernameMsg==null) usernameMsg="";
		if(passwordMsg==null) passwordMsg="";
		
		String username = (String)session.getAttribute("username");
		if(username!=null && !("".equals(username)))
		{
			//如果已经登录
			session.setAttribute("username", "");
		}
		
		
	%>
	
	<!-- 标题框 -->
	<ul class="layui-nav" layui-bg-cyan lay-filter="">
		<li class="layui-nav-item" style="padding:5px" ><img  src="images/logo.png"> </li>
		 	 
	</ul>

	<hr class="layui-bg-gray">

	<!-- 轮播图片 -->
	<div align=center>
		<div class="layui-carousel" id="test1" align=center>
	 		 <div carousel-item =''>
	   			<div><img src="images/chd_01.jpg"></div>
	   			<div><img src="images/chd_02.jpg"></div>
			    <div><img src="images/chd_03.jpg"></div>
			    <div><img src="images/chd_04.jpg"></div>
			    <div><img src="images/chd_05.jpg"></div>
			    <div><img src="images/chd_06.jpg"></div>
			  </div>
		</div>
	</div>
	
	<script>
		
		layui.use('carousel', function(){
		  var carousel = layui.carousel;
		  //建造实例
		  carousel.render({
		    elem: '#test1'
		    ,width: '960px' 		//设置容器宽度
		    ,height: '290px'		//按比例和可视页面宽度来获取高度
		    ,arrow: 'always' 	//始终显示箭头
		    ,autoplay:'true'	//自动播放
		    ,interval:3000		//自动时间间隔
		  });
		});
		
		//窗口发生变化时重新加载
		$(window).resize(function(){
			window.location.reload()
		})
		
	</script>
	
	<br>
	
	<!-- 登陆模块 -->
	<div class="layui-row layui-col-space10" >
		<div class="layui-col-md4"></div>
		<div class="layui-col-md4"  >
		
			<div style="margin: 5px; padding: 5px; width: 400px" align=center>
				<h1>学生信息管理系统</h1>
			</div>
			
			
			<form action="AccountVerifyServlet" method="POST" class="layui-form">
			
				<input type="hidden" name="method" value="login">
				<div class="layui-form-item" style="width: 400px">
					<label class="layui-form-label">用户名</label>
					<div class="layui-input-block">
						<input type="text" name="username" required lay-verify="required"
							placeholder="请输入用户名" autocomplete="off" class="layui-input">
							
							
						<div class="layui-form-mid layui-word-aux">
						<%=usernameMsg %>
						</div>
					</div>
				</div>
				<div class="layui-form-item" style="width: 400px">
					<label class="layui-form-label">密码</label>
					<div class="layui-input-block">
						<input type="password" name="password" required
							lay-verify="required" placeholder="请输入密码" autocomplete="off"
							class="layui-input">
							
						<div class="layui-form-mid layui-word-aux">
						<%=passwordMsg %>
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
						
						<a href="register.jsp" class="layui-btn layui-btn-primary">注册</a>
						
					</div>
				</div>
			</form>

		</div>
		
		<div class="layui-col-md4" ></div>
	</div>
	
	<div class="layui-row" align=center style="margin-top:150px">
		<div class="layui-footer layui-bg-gray "  >
		  	<p>- ©2020-Web课设小组 保留所有权利 -</p>
		</div>
	</div>
</body>
</html>