<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>学生信息管理系统</title>
	<!-- 引入示例，每个用到layui的文件在头部加上这两行 -->
	<link rel="stylesheet" href="layui/css/layui.css">
	<script src="layui/layui.js"></script>
	<script>
		//注意：导航 依赖 element 模块，否则无法进行功能性操作
		layui.use('element', function(){
	  	var element = layui.element;
	  
	  	//…
		});
	</script>
</head>
<body class="layui-layout-body">
	<div class="layui-layout layui-layout-admin">
	  <div class="layui-header">
		<div class="layui-logo">学生信息管理系统</div>
		<!-- 头部区域（可配合layui已有的水平导航） -->
		
		<ul class="layui-nav layui-layout-right">
		  <li class="layui-nav-item">
			<a href="javascript:;"><img src="https://s2.ax1x.com/2019/12/30/lQghKH.jpg" class="layui-nav-img">
				<%
				String username = (String)session.getAttribute("username");
				if(username==null)
				{
					username="";
				}
				if("".equals(username)){
					//如果没有登录，那么跳转到登录页面
					%>
					<% response.sendRedirect("login.jsp"); %>
					<% 
				}
				
				%>
				<%=username %>
			</a>
			<dl class="layui-nav-child">
			  <dd><a href="javascript:;">基本资料</a></dd>
			  <dd><a href="javascript:;">安全设置</a></dd>
			</dl>
		  </li>
		  <li class="layui-nav-item"><a href="login.jsp">注销</a></li>
		</ul>
	  </div>
	  
	  <div class="layui-side layui-bg-black">
		<div class="layui-side-scroll">
		<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
		  	<ul class="layui-nav layui-nav-tree" lay-filter="test">
		  		<li class="layui-nav-item"><a href="student_info.jsp" target="frame"><i class="layui-icon layui-icon-username"></i> 显示所有学生信息</a></li>
				<li class="layui-nav-item"><a href="student_search.jsp" target="frame"><i class="layui-icon layui-icon-read"></i> 学生信息查询</a></li>
				<li class="layui-nav-item"><a href="student_update.jsp" target="frame"><i class="layui-icon layui-icon-add-circle"></i> 学生信息添加</a></li>
				<li class="layui-nav-item"><a href="welcome.jsp" target="frame"><i class="layui-icon layui-icon-group"></i> 关于</a></li>
			</ul> 
		</div>
	  </div>
	  
	  <div class="layui-body">
		<!-- 内容主体区域 -->
		<iframe src="welcome.jsp" name="frame" width=100% height=100% id="content"></iframe>
	  </div>
	  
	  <div class="layui-footer">
	  	<p>©2020-Web课设小组 保留所有权利</p>
	  </div>
	</div>
</body>
</html>