<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>学生信息查询</title>
	<link rel="stylesheet" href="layui/css/layui.css">
	<link rel="stylesheet" href="soulTable.css" media="all"/>
	<script src="layui/layui.js"></script>
</head>
<body>
	<h2 align=center>学生信息查询</h2>
	<fieldset class="layui-elem-field">
		<legend>按学号查询</legend>
		<form class="layui-form" name="base-number" method="post" action="student_info.jsp">
		<input type="hidden" name="operation" value="queryById"> 
			<div class="layui-form-item">
			    <label class="layui-form-label">输入学号</label>
			    <div class="layui-input-block">
			    	<input type="text" name="studentId" required  lay-verify="required" placeholder="请输入学号" autocomplete="off" class="layui-input">
			    </div>
	  		</div>
	  		<div class="layui-form-item">
	    		<div class="layui-input-block">
		     		<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
		      		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
	    		</div>
	 		</div>
		</form>
	</fieldset>
	<fieldset class="layui-elem-field">
		<legend>按姓名查询</legend>
		<form class="layui-form" name="base-name" method="post" action="student_info.jsp">
		<input type="hidden" name="operation" value="queryByName"> 
			<div class="layui-form-item">
			    <label class="layui-form-label">输入姓名</label>
			    <div class="layui-input-block">
			    	<input type="text" name="name" required  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input">
			    	
			    </div>
	  		</div>
	  		<div class="layui-form-item">
	    		<div class="layui-input-block">
		     		<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
		      		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
	    		</div>
	 		</div>
		</form>
	</fieldset>
	
</body>
</html>