package studentInfo;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Servlet implementation class StudentDataServlet
 */
@WebServlet("/StudentDataServlet")
public class StudentDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	StudentDataQueryBean studentQuery = new StudentDataQueryBean();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentDataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 响应参数格式设置
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_OK);
		
		// 获取参数
		
		/*
		 * 操作类型String type
			学号String studentId
			姓名String name
			性别boolean gender
			班级String classId
			专业String major
			学生类别String type
			入学日期Date enrollDate
			学制int academicStructure
			所属校区String campus * 
		 * */
		
		String operation = request.getParameter("operation");
		String studentId = request.getParameter("studentId");
		String name = request.getParameter("name");
		String genderString = request.getParameter("gender");
		String classId = request.getParameter("classId");
		String major = request.getParameter("major");
		String type = request.getParameter("type");
		String enrollDateString = request.getParameter("enrollDate");
		String academicStructureString = request.getParameter("academicStructure");
		String campus = request.getParameter("campus");
		
		//对每个参数进行预处理，将null的参数置为空字符串
		if(operation==null) operation="";//如果操作类型为空，则直接返回，不进行操作
		if(studentId==null) studentId="";
		if(name==null) name="";
		if(genderString==null) genderString="";
		if(classId==null) classId="";
		if(major==null) major="";
		if(type==null) type="";
		if(enrollDateString==null) enrollDateString="2020-06-09";
		if(academicStructureString==null) academicStructureString="0";
		if(campus==null) campus="";
		
		//将参数转换为它对应的类型
		boolean gender = genderString.equals("男")?true:false;
		
		SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
		Date enrollDate = null;
		try {
			enrollDate = sdf.parse(enrollDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		int academicStructure = Integer.valueOf(academicStructureString);
		
		//根据操作类型进行不同操作
		String jsonString = "";
		switch (operation) {
		case "queryById"://按照学号查询学生信息
			jsonString = queryById(studentId);
			break;
			
		case "queryByName"://按照姓名查询学生信息
			jsonString = queryByName(name);
			break;
			
		case "add"://添加学生信息
			jsonString = add(studentId, name, gender, classId, major, type, enrollDate, academicStructure, campus);
			request.getRequestDispatcher("student_update.jsp").forward(request, response);
			break;
			
		case "mod"://修改学生信息
			jsonString = mod(studentId, name, gender, classId, major, type, enrollDate, academicStructure, campus);
			request.getRequestDispatcher("student_update.jsp").forward(request, response);
			break;
			
		case "del"://删除学生信息
			jsonString = del(studentId);
			break;

		default: case "queryAll"://查询全部学生信息
			jsonString = queryAll();
			break;
		}
		
		response.getWriter().print(jsonString);
		
	}
	
	/**
	 * 将学生信息数组打包成符合layui数据表格规定的json字符串
	 * @param students 学生信息数组
	 * @return 符合layui数据表格规定的json字符串
	 */
	public String getLayuiJson(StudentDataBean[] students) {
		int code = 0;
		String msg = "";
		int count = students==null?0:students.length;
		
		return getLayuiJson(students, code, msg, count);
	}
	
	/**
	 * 将学生信息数组打包成符合layui数据表格规定的json字符串
	 * @param students 学生信息数组
	 * @param code 状态码
	 * @param msg 提示信息
	 * @param count 数据项数目
	 * @return 符合layui数据表格规定的json字符串
	 */
	public String getLayuiJson(StudentDataBean[] students,int code,String msg,int count) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("code",code);//如果查询不到，则返回null
		jsonObject.put("msg",msg);
		jsonObject.put("count",count);
		
		JSONArray dataList = new JSONArray();
		if(students!=null) {
			for(StudentDataBean student:students) {
				//将javabean转换为JSONObject
				JSONObject data = JSON.parseObject(JSON.toJSONString(student));
				dataList.add(data);
			}
		}
		jsonObject.put("data",dataList);
		return jsonObject.toJSONString();
	}
	
	
	/**
	 * 查询所有学生信息
	 * @return 符合layui数据表格规定的json字符串
	 */
	public String queryAll() {
		
		try {
			
			
			//查询所有学生信息
			StudentDataBean[] students = studentQuery.queryAllData();
			
			return getLayuiJson(students);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return getLayuiJson(null);
	}
	
	/**
	 * 按照学号查询
	 * @param studentId 学号
	 * @return 符合layui数据表格规定的json字符串
	 */
	public String queryById(String studentId) {
		try {
			
			//按照学号查询
			StudentDataBean[] students = studentQuery.queryById(studentId);
			
			return getLayuiJson(students);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return getLayuiJson(null);
	}
	
	/**
	 * 按照姓名查询
	 * @param name 姓名
	 * @return 符合layui数据表格规定的json字符串
	 */
	public String queryByName(String name) {
		try {
			
			//按照姓名查询
			StudentDataBean[] students = studentQuery.queryByName(name);
			
			return getLayuiJson(students);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return getLayuiJson(null);
	}
	
	/**
	 * 添加学生信息
	 * @param studentId 学号
	 * @param name 姓名
	 * @param gender 性别
	 * @param classId 班级
	 * @param major 专业
	 * @param type 类型
	 * @param enrollDate 入学日期
	 * @param academicStructure 学制
	 * @param campus 校区
	 * @return 符合layui数据表格规定的json字符串
	 */
	public String add(
			String studentId,String name,boolean gender,
			String classId,String major,String type,
			Date enrollDate,int academicStructure,String campus) {
		
		
		StudentDataBean student = new StudentDataBean(studentId, name, gender, classId, major, type, enrollDate, academicStructure, campus);
		StudentDataBean[] students = new StudentDataBean[] {student};
		int code = studentQuery.addData(student)?0:-1;

		return getLayuiJson(students, code, "", 0);
	}
	
	/**
	 * 修改学生信息
	 * @param studentId 学号
	 * @param name 姓名
	 * @param gender 性别
	 * @param classId 班级
	 * @param major 专业
	 * @param type 类型
	 * @param enrollDate 入学日期
	 * @param academicStructure 学制
	 * @param campus 校区
	 * @return 符合layui数据表格规定的json字符串
	 */
	public String mod(
			String studentId,String name,boolean gender,
			String classId,String major,String type,
			Date enrollDate,int academicStructure,String campus) {
		
		
		StudentDataBean student = new StudentDataBean(studentId, name, gender, classId, major, type, enrollDate, academicStructure, campus);
		StudentDataBean[] students = new StudentDataBean[] {student};
		int code = studentQuery.modData(studentId,student)?0:-1;

		return getLayuiJson(students, code, "", 0);
	}
	
	/**
	 * 删除学生信息
	 * @param studentId 学号
	 * @return 符合layui数据表格规定的json字符串。返回的学生信息是删除前的学生信息
	 */
	public String del(String studentId) {
		try {
			StudentDataBean[] students = studentQuery.queryById(studentId);
			int code = studentQuery.delData(studentId)?0:-1;
			return getLayuiJson(students, code, "", 0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return getLayuiJson(null);
	}

}
