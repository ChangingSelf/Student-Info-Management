package studentInfo;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AccountVerifyServlet
 */
@WebServlet("/AccountVerifyServlet")
public class AccountVerifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountVerifyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub		
			response.setCharacterEncoding("UTF-8");
			PrintWriter out=response.getWriter();
			HttpSession session =request.getSession();		
			
			AccountVerifyBean userBean = new AccountVerifyBean();	
			String method = request.getParameter("method");
			if("login".equals(method)){//登录
				String username = request.getParameter("username");
				String password = request.getParameter("password");
				
				if(username==null||"".equals(username.trim())||password==null||"".equals(password.trim())){
					request.getRequestDispatcher("login.jsp").forward(request, response);  
					return;
				}
				
				int code;
				try {
					code = userBean.verify(username,password);
					switch (code) {
					case 0://登录成功，跳转到主页
						session.setAttribute("username", username);
						request.getRequestDispatcher("index.jsp").forward(request, response);  
						break;
					case -1://用户名不存在，跳转回登录页面
						session.setAttribute("usernameMsg", "用户名不存在");
						session.setAttribute("passwordMsg", "");
						request.getRequestDispatcher("login.jsp").forward(request, response);
						break;
					case -2://密码与用户名不匹配,跳转回登录页面
						session.setAttribute("usernameMsg", "");
						session.setAttribute("passwordMsg", "密码与用户名不匹配");
						request.getRequestDispatcher("login.jsp").forward(request, response);
						break;
					default:
						request.getRequestDispatcher("login.jsp").forward(request, response);
						break;
					}
				} catch (SQLException e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
			
			}
			else if("register".equals(method)){//注册
				String username = request.getParameter("username");
				String password1 = request.getParameter("password1");
				String password2 = request.getParameter("password2");
				if(username==null||"".equals(username.trim())||password1==null||"".equals(password1.trim())||password2==null||"".equals(password2.trim())||!password1.equals(password2))
				{
					System.out.println("用户名或密码不能为空！");
					response.sendRedirect("register.jsp");
					return;
				}
				boolean isExit = userBean.isExist(username);
				if(!isExit){
					if(userBean.add(username, password1))
					{
						out.println("注册成功，请登录！");
						response.sendRedirect("login.jsp");
						return;
					}
					
				}
				else{
					out.println("用户名已存在！");
					response.sendRedirect("register.jsp");
					return;
				}
				
			}
			else{
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
	}
}
