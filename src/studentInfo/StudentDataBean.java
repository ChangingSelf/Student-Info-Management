package studentInfo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 存储学生数据
 * @author Samey
 *
 */
public class StudentDataBean {
	
	private String studentId;	//学号
	private String name;		//姓名
	private boolean gender;		//性别--男true；女false

	private String classId;		//班级
	private String major;		//专业
	private String type;		//学生类别 本科生or研究生
	
	private Date enrollDate;	//入学日期
	private int academicStructure;		//学制
	private String campus;		//所属校区
	
	
	public StudentDataBean() {
		
	}
	
	public StudentDataBean(String studentId, String name, boolean gender, String classId, String major, String type,
			Date enrollDate, int academicStructure, String campus) {
		super();
		this.studentId = studentId;
		this.name = name;
		this.gender = gender;
		this.classId = classId;
		this.major = major;
		this.type = type;
		this.enrollDate = enrollDate;
		this.academicStructure = academicStructure;
		this.campus = campus;
	}

	/**
	 * 将所有属性打印到控制台，用于测试
	 */
	public void printData() {
		System.out.println("---------------------------");
		System.out.println("学号：" +this.studentId);
		System.out.println("姓名：" +this.name);
		System.out.println("性别：" +this.getGenderString());
		
		System.out.println("班级：" +this.classId);
		System.out.println("专业：" +this.major);
		System.out.println("学生类别：" +this.type);
		
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd"); 
		System.out.println("入学日期：" +df.format(this.enrollDate));
		
		System.out.println("学制：" +this.academicStructure);
		System.out.println("所属校区：" +this.campus);
		System.out.println("---------------------------");
	}
	
	/**
	 * 输出性别字符串
	 * @return ture = “男”；false = “女”
	 */
	public String getGenderString() {
		if (this.gender)
			return "男";
		else
			return "女";
	}
	
	/*---系统生成的get和set函数---*/
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isGender() {
		return gender;
	}
	public void setGender(boolean gender) {
		this.gender = gender;
	}
	public void setGender(String gender) {
		if(gender==null) this.gender=false;
		this.gender = gender.equals("男")?true:false;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getEnrollDate() {
		return enrollDate;
	}
	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}
	public int getAcademicStructure() {
		return academicStructure;
	}
	public void setAcademicStructure(int academicStructure) {
		this.academicStructure = academicStructure;
	}
	public String getCampus() {
		return campus;
	}
	public void setCampus(String campus) {
		this.campus = campus;
	}
}
