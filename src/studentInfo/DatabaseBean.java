package studentInfo;

import java.sql.*;

/**
 * 连接数据库并提供查询操作
 * @author Samey
 *
 */
public class DatabaseBean {
	public DatabaseBean() {
		conn=connect();
	}
	private Connection conn = null;		//数据库连接
	private Statement stmt = null;		//语句句柄
	private String dbName = null;		//数据库名
	
	//系统生成的getter和setter
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public Statement getStmt() {
		return stmt;
	}

	public void setStmt(Statement stmt) {
		this.stmt = stmt;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * 构造函数里面用此函数连接数据库
	 * @param username 用户名
	 * @param password 密码
	 * @param dbname 	数据库名
	 * @param tableName 表名？不知道怎么用
	 * @return
	 */
	public static Connection connect(String username,String password,String dbname,String tableName)
	{
		//TODO
		Connection con = null;
		String driverName = "com.mysql.cj.jdbc.Driver";	//MySQL驱动程序？ 8.x版本的
		String url = "jdbc:mysql://localhost:3306/"+dbname+"?&serverTimezone=GMT%2B8";//连接数据库的URL
		try
		{
			
			Class.forName(driverName);	
			System.out.println("加载成功");
		}catch(ClassNotFoundException e)
		{
			System.out.print("Error loading Driver,不能加载驱动程序！");
		}
		
		try
		{
			con = DriverManager.getConnection(url,username,password);
		}
		catch(SQLException er)
		{
			System.out.print("Error getConnection,不能连接数据库！");
		}
		return con;
	}
	
	/**
	 * 使用默认参数调用有参数的对应函数
	 * @return
	 */
	public static Connection connect()
	{
		//TODO:默认参数版
		Connection con = null;
		String driverName = "com.mysql.cj.jdbc.Driver";	//MySQL驱动程序？
		String dbname = "student_info";
		String url = "jdbc:mysql://localhost:3306/"+dbname+"?&serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=UTF-8";//连接数据库的URL？？？
		String username="root";
		String password="1234";
		
		try
		{
			
			Class.forName(driverName);	
			System.out.println("加载成功");
		}catch(ClassNotFoundException e)
		{
			System.out.print("Error loading Driver,不能加载驱动程序！");
		}
		
		try
		{
			con = DriverManager.getConnection(url,username,password);
		}
		catch(SQLException er)
		{
			System.out.print("Error getConnection,不能连接数据库！");
		}
		return con;
		
	}
	
	/**
	 * 执行查询操作
	 * @param sql
	 * @return 查询结果集ResultSet对象
	 */
	public ResultSet executeQuery(String sql)
	{
		//TODO
		ResultSet rst = null;
		try
		{
			stmt=conn.createStatement();	//建立Statement对象
			rst=stmt.executeQuery(sql);		//执行SQL语句
			
		}
		catch(SQLException ex)
		{
			System.err.print("Error Execute Query:"+ex.getMessage());
		}
		return rst;
	}
	
	/**
	 * 执行增删改查
	 * @param sql
	 */
	
	//修改
	public boolean executeUpdate(String sql) {
		//TODO
		try
		{
			stmt=conn.createStatement();	//建立Statement对象
			stmt.executeUpdate(sql);			//执行SQL语句
			
		}
		catch(SQLException ex)
		{
			System.err.println(ex.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * 关闭各种资源
	 */
	public void close() {
		//TODO
		try
		{
			stmt.close();
			conn.close();
		}
		catch(SQLException ex){
			System.err.println(ex.getMessage());
		}
	}
}
