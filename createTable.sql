create database student_info;

use student_info;

#--创建账户信息表
create table Account
(
	username char(20) primary key,
	password char(30)
);

#--创建学生信息表
create table Student
(	
	Sno char(20) primary key,
	Sname char(30),
	Ssex char(2),
	Sclassid char(20),
	Smajor char(50),
	Stype char(30),
	SenrollDate DATE,
	SacademicStructure int,
	Campus char(30)
);

insert
into Student(Sno,Sname,Ssex,Sclassid,Smajor,Stype,SenrollDate,SacademicStructure,Campus)
values
('2017901006','杨啸','男','2017240206','软件工程','本科生','2017-09-01',4,'渭水校区'),
('2017901007','李青翰','男','2017240206','软件工程','本科生','2017-09-01',4,'渭水校区'),
('2017901008','赵念豫','女','2017240206','软件工程','本科生','2017-09-01',4,'渭水校区'),
('2017901009','陈佑辉','男','2017240207','软件工程','本科生','2017-09-01',4,'渭水校区');

insert
into Account(username,password)
values('admin','1234');